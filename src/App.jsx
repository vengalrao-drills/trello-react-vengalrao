import React, { useEffect, useState } from 'react';
import { Typography } from '@mui/material';
import useStyles from './Styles/styles';
import { Box } from '@mui/material';
import BoardsDisplay from './Components/BoardsDisplay';
import CircularProgress from '@mui/material/CircularProgress';
import Header from './Components/Header';
import { getData, postNewBoard } from './Data/key'; 

function App() {
  const classes = useStyles();
  const [data, setData] = useState([]); 
  const [boardName, setBoardName] = useState('');
  const [loadingData, setLoadingData] = useState(true);
  const [error, setError] = useState(false);

    const fetchData = async () =>{
      try {
        let answer = await getData();
        setData(answer)
        setLoadingData(false);
      }catch(error){
        setError(error)
        setLoadingData(false)
      }
    } 

  useEffect(() => {
    fetchData()
  }, []);
 
  const handleChange = (e) => {
    if (e.target.name == 'boardName') {
      setBoardName(e.target.value);
    }
  }

  const PostBoardData =async () => {
    try{
    let answer = await postNewBoard(boardName);
    setBoardName('');
    setData((prev)=>[...prev , answer])
    setLoadingData(false)
    }catch(e){
      setError(true); console.log('err')
    }
  }


  return (
    <Box className={classes.root}>
      <Header/>
      {error ?
        <Typography>Error 404 Not Found</Typography> :
        <>
          {loadingData ? <CircularProgress className={classes.circularLoading}  color="primary"  variant="indeterminate"  /> 
          : 
          <>
          <BoardsDisplay getData={getData} PostBoardData={PostBoardData} handleChange={handleChange} boardName={boardName} data={data} setData={setData}    />
          </>
          
          }
        </>
      }
    </Box>
  );
}

export default App;


