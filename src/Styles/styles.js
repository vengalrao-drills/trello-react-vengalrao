import { makeStyles } from "@mui/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    fontFamily: "sans-serif",
    padding:'0',
    margin:'0',
    boxSizing:'border-box'
  },
  title: {
    padding: "1rem",
  },
  boardsContainer: {
    display: "flex", 
    flexWrap: "nowrap",
    overflow:"scroll"
  },
  Allboards: {
    height: "10rem",
    margin: "1rem",
    display: "flex",
    flexDirection:'column', 
    borderRadius:"1rem",
    padding:"1.3rem ",
    background:"#988f2a",


    
  }, 
  CloseIcon:{ 
    color:"wheat",
    opacity:"0"
  },

  l:{
    color:"white",
    backgroundColor:"#23231A",
    userSelect:'none',
    cursor:"pointer"
  }, 
  
  boardsNames:{
    width:"15rem"
  },

  
  text:{ 
    textAlign:"center", 
    padding:"2rem", 
  },

  inputBox:{
    display:"flex",
    flexDirection:'column', 
    width:"50%",
    margin:'auto', 
  },    

  circularLoading:{
    position:"absolute",
    left:"50%",
    top:"30%",
    transform:"translate(-50%,-50%)", 
  },

  cardList:{  
    padding:'1rem',
    width:"25rem"

  },

  AllLists:{
    display:"flex",
    flexDirection:"row",
    overflow:"scroll", 
  },

  a:{
    margin:"2rem",
    width:"20rem",
    background:"red"
  }

  , checklistsDisplay:{
      background:"red",
      border:"2px solid red",
      margin:"0 auto"
  }

}));

export default useStyles;
