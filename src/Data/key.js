let key = "f0dbc18a97b16906dbf036eb03015fab";
let APIToken =
  "ATTAe4d80247b20b4ff017c22fded3b8f0d8dd51ade4b55eef5998c3a1541dc398f95DF3BB18";

const exportData = {
  key: key,
  APIToken: APIToken,
};

export default exportData;

import axios from "axios";
let url = "https://api.trello.com/1";

const getData = async () => {
  const baseUrl = `${url}/members/me/boards?key=${key}&token=${APIToken}`;
  try {
    let response = await axios.get(baseUrl);
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
};

const postNewBoard = async (boardName) => {
  try {
    let response = await axios.post(
      `${url}/boards/?name=${boardName}&key=${key}&token=${APIToken}`
    );
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
};

const deleteBoardId = async (id) => {
  try {
    let response = await axios.delete(
      `${url}/boards/${id}?key=${key}&token=${APIToken}`
    );
    return response;
  } catch (error) {
    throw new Error(error);
  }
};

const getLists = async (id) => {
  try {
    let response = await axios.get(
      `${url}/boards/${id}/lists?key=${key}&token=${APIToken}`
    );
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
};

const createCardd = async (newCard, id) => {
  try {
    let response = await axios.post(
      `${url}/cards?name=${newCard}&idList=${id}&key=${key}&token=${APIToken}`
    );
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
};

const addNewChecklistt = async (ChkItemidList, newCard) => {
  try {
    let response = await axios.post(
      `https://api.trello.com/1/checklists/${ChkItemidList}/checkItems?name=${newCard}&key=${key}&token=${APIToken}`
    );
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
};

const addNewListt = async (currentBoardId, newCard) => {
  try {
    let response = await axios.post(
      `https://api.trello.com/1/boards/${currentBoardId}/lists?name=${newCard}&key=${key}&token=${APIToken}`
    );
    return response.data;
  }catch (error) {
    throw new Error(error);
  }
};

const getCardss = async (id) => {
  try {
    let response = await axios.get(
      `https://api.trello.com/1/lists/${id}/cards?key=${key}&token=${APIToken}`
    );
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
};

const deleteCardd = async (ID) => {
  try {
    let response = await axios.delete(
      `https://api.trello.com/1/cards/${ID}?key=${key}&token=${APIToken}`
    );
    return response;
  } catch (error) {
    throw new Error(error);
  }
};

const getCheckListss = async (cardId) => {
  try {
    let response = await axios.get(
      `https://api.trello.com/1/cards/${cardId}/checklists?key=${key}&token=${APIToken}`
    );
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
};

const createCheckListt = async (cardId, name) => {
  try {
    let response = await axios.post(
      `https://api.trello.com/1/cards/${cardId}/checklists?name=${name}&key=${key}&token=${APIToken}`
    );
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
};

const deleteCheckListt = async (cardId, idCheckItem) => {
  try {
    let response = await axios.delete(
      `https://api.trello.com/1/cards/${cardId}/checklists/${idCheckItem}?key=${key}&token=${APIToken}`
    );
    return response;
  } catch (error) {
    throw new Error(error);
  }
};

const getCheckItemss = async (work) => {
  try {
    let response = await axios.get(
      `https://api.trello.com/1/checklists/${work.id}/checkItems?key=${key}&token=${APIToken}`
    );
    // console.log('response - - ', response)
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
};

const updateChkItemm = async (idCard, idChecklist, idCheckItem, state) => {
  try {
    let response = await axios.put(
      `https://api.trello.com/1/cards/${idCard}/checklist/${idChecklist}/checkItem/${idCheckItem}?key=${key}&token=${APIToken}&state=${state}`
    );
    return response.data;
  } catch (error) {
    throw new Error(error);
  }
};

const deleteCheckItemm = async (checkItemid) => {
  try {
    let response = await axios.delete(
      `https://api.trello.com/1/checklists/${checkItemid.idChecklist}/checkItems/${checkItemid.id}?key=${key}&token=${APIToken}`
    );
    return response;
  } catch (error) {
    throw new Error(error);
  }
};

const deleteListt = async (id)=>{
  try{
    let response = await axios.put(`https://api.trello.com/1/lists/${id}/closed?value=true&key=${key}&token=${APIToken}`);
    return response
  }catch(error){
    throw new Error(error);
  }
}

export {
  getData,
  postNewBoard,
  deleteBoardId,
  getLists,
  createCardd,
  addNewChecklistt,
  addNewListt,
  getCardss,
  deleteCardd,
  getCheckListss,
  createCheckListt,
  deleteCheckListt,
  getCheckItemss,
  updateChkItemm,
  deleteCheckItemm,
  deleteListt
};
