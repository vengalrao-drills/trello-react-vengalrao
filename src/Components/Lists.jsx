import React, { useEffect, useState } from 'react'
import { useLocation, useParams } from 'react-router-dom'
import Header from './Header';
import { Typography } from '@mui/material';
import { Box } from '@mui/material';
import CardMui from './CardMui';
import { getLists } from '../Data/key';
import Loading from '../MuiUsable/Loading';
import PageNotFound from './PageNotFound';


const Lists = () => {
    const location = useLocation();
    let currentBoardName = location.state

    let { id } = useParams();
    let [lists, setLists] = useState([]);
    const [error, setError] = useState(false);
    const [loading, setLoading] = useState(true);
    const fetchLists = async () => {
        try {
            let response = await getLists(id);
            console.log('response', response)
            setLists(response)
            setLoading(false)
        } catch (e) {
            setLoading(false)
            setError(true);
        }
    }

    useEffect(() => {
        fetchLists()
    }, [id])


console.log('error' , error)
    return ( 

        <div>
            {error ? (<PageNotFound />) : (
                <>
                    <Header />
                    {loading ? (
                        <Box sx={{ display: "flex", justifyContent: "center", position: "absolute", top: "50%", left: "50%", transform: "translate(-50%,-50%)" }}>
                            <Loading />
                        </Box>
                    ) : (
                        <>
                            <Box>
                                <Box sx={{ display: "flex", alignItems: "baseline" }}>
                                    <Typography variant='h3' sx={{ margin: "2rem auto", textAlign: "center" }} >
                                        {currentBoardName}
                                    </Typography>
                                </Box>
                                {/* CardMui - Displays cards (below the lists) */}
                                <CardMui lists={lists} setLists={setLists} />
                            </Box>
                        </>
                    )}
                </>
            )}
        </div>

    )
}

export default Lists
