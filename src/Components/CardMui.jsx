import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import useStyles from '../Styles/styles';
import MoreHorizIcon from '@mui/icons-material/MoreHoriz';
import CardData from './CardData'; 
import DeleteOption from '../MuiUsable/DeleteOption';
import { useParams } from 'react-router-dom';
import Toast from '../MuiUsable/Toast';
import Change from '../MuiUsable/ChangeInput'
import { deleteListt } from '../Data/key';

export default function CardMui({ lists  , setLists }) { 
  const classes = useStyles(); 
  const [error , setError] = React.useState('');

  const deleteIt =  async  (id) => {
    try{
      let response =await deleteListt(id);
      if(response.status!=200){
        throw new Error('401 error -  Cannot Delete List')
      }else{
        setLists(lists.filter(list=>list.id!=id));
      }
    }catch(e){
      setError('401 error -  Cannot Delete List')
    }
  }
  let params = useParams();
  let currentBoardId = params.id;
  let allLists = '';
 
 

  if (lists[0]) {
    allLists = lists.map((list) => (
      <Card key={list.id} variant="outlined" sx={{ background: '#F1F2F4', alignSelf: 'flex-start', marginRight: "1rem",  minWidth:"20rem"   }} className={classes.cardList}>
        <Typography gutterBottom variant="h5" sx={{ margin: "0 0 1rem 0", display: "flex", flexDirection: "row", justifyContent: "space-between" }} component="div">
          {list.name}  
          <DeleteOption deleteIt={deleteIt} id={list.id} >
            <MoreHorizIcon sx={{ textAlign: "right", cursor: "pointer" }} /> 
          </DeleteOption>
        </Typography>
        <CardData id={list.id}  />
      </Card>
    ))
  }

  let addAnotherList = <Box variant="outlined" sx={{ background: '#e8e6e6', color: "black", alignSelf: 'flex-start', minWidth: "20rem",     }} className={classes.cardList} >
    <Typography gutterBottom variant="h5" component="div">
      <Change start={"Enter List Name"}  text={"Enter List Name"} functionName={'addNewList'} submitMatter={"Add"} currentBoardId={currentBoardId}  setLists={setLists} />
    </Typography>
  </Box>

  return (

    <Box sx={{ display: 'flex', overflowX: "auto" }}>
      {allLists}
      {addAnotherList} 
      {  error.length>0 ?  <Toast  error={error} setError={setError} /> :"" }      
    </Box>
  );
}









 