import React, { useEffect, useState } from 'react'
import { Card, Typography } from '@mui/material';
import { CardContent } from '@mui/material';
import { Button } from '@mui/material';
import { Box } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import CheckListsDisplay from './CheckListsDisplay.jsx';
import Toast from '../MuiUsable/Toast';
import Loading from '../MuiUsable/Loading';
import Change from '../MuiUsable/ChangeInput.jsx'; 
import { getCardss , deleteCardd } from '../Data/key';

const CardData = ({ id }) => {
    let [cardsData, setCardsData] = useState([]);
    const [error, setError] = useState('');
    const [loading, setLoading] = useState(true);
    const getCards = async () => { 
        try{
            let response = await getCardss(id);
            setCardsData(response);
            setLoading(false)            
        }catch(e){
            setError('404 Error - Cards not Fetched')
        }
    }
    useEffect(() => {
        getCards()
    }, [])

    const deleteCard = async  (ID) => {
        try{
            let response = await  deleteCardd(ID);
            if(response.status==200){
                console.log('response' , response)
                let answer = cardsData.filter(card => card.id !== ID);
                setCardsData(answer);
            }
        }catch(e){
            setError('404 Error -  Card Not Deleted')
        }
    }

    let cardDataDisplay = '';
    if (cardsData.length > 0) {
        cardDataDisplay = cardsData.map((card) => {
            return (
                <Box key={card.id}   >
                    <Card sx={{ margin: "0.9rem 0rem ", borderRadius: "0.5rem" }}  >
                        <CardContent className='{classes}' sx={{ display: "flex", flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}  >
                            <CheckListsDisplay  cardId={card.id} cardName={card.name}   >
                                <Typography variant='h6' sx={{}} onClick={() => getCards()} >{card.name}   </Typography>
                            </CheckListsDisplay>
                            <Button onClick={() => { deleteCard(card.id) }}  >  <DeleteIcon sx={{ color: "black" }} />   </Button>
                        </CardContent>
                    </Card>
                </Box>
            )
        });
    }

    return (
        <div>
            {loading? <Loading /> :
                <>
                    {cardDataDisplay}
                    {/* Input - - - - [Change] is Input */}
                    <Change functionName = {'createCard'}   start={'Add a card'} text={'Enter Card Name '} submitMatter={'Add Card'} setCardsData={setCardsData} id={id}  />
                    {error.length > 0 ? <Toast error={error} setError={setError} /> : ""}
                </>
            }
        </div>
    )
}

export default CardData  
