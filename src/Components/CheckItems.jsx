import { Box } from '@mui/material';
import React, { useEffect, useState } from 'react';
import Typography from '@mui/material/Typography';
import Checkbox from '@mui/material/Checkbox';
import { Card } from '@mui/material'; 
import Toast from '../MuiUsable/Toast';
import Change from '../MuiUsable/ChangeInput';
import MoreHorizIcon from '@mui/icons-material/MoreHoriz';
import DeleteOption from '../MuiUsable/DeleteOption';
import Loading from '../MuiUsable/Loading';
import ProgressBar from '../MuiUsable/ProgressBar';
import { getCheckItemss, updateChkItemm, deleteCheckItemm } from '../Data/key';

const CheckItems = ({ work  }) => {
  const [item, setItem] = useState([]); 
  const [error, setError] = useState('');
  const label = { inputProps: { 'aria-label': 'Checkbox demo' } };

  const [chkStates, setChkStates] = useState();
  const [loading, setLoading] = useState(true);


  const getCheckItems = async () => {
    try {
      let response = await getCheckItemss(work);
      setLoading(false);
      setItem(response);

      let values = [];
      response.forEach((response) => {
        if (response.state == 'complete') {
          values.push(true)
        } else {
          values.push(false)
        }
      })
      setChkStates(values);
    } catch (e) {
      setError(e)
    }
  }


  useEffect(() => {
    getCheckItems();
  }, []);

  let newChkStates = [];
  let oldChckStates = [];
  const handleToggle = (indexi, chkItem) => {
    let state = ''
    oldChckStates = [...chkStates];
    newChkStates = chkStates.map((i, index) => {
      if (index == indexi) {
        if (i) {
          state = 'incomplete'
        } else {
          state = 'complete'
        }
        return !i
      } else {
        return i
      }
    })
    setChkStates(newChkStates);
    updateChkItem(work.idCard, chkItem.idChecklist, chkItem.id, state);
  }

  const updateChkItem = async (idCard, idChecklist, idCheckItem, state) => {
    try {
      let response = await updateChkItemm(idCard, idChecklist, idCheckItem, state);
    } catch (e) {
      setError('404 Error - item not Updated')
      setChkStates(oldChckStates)
    }
  }

  const deleteCheckItem = async (checkItemid) => {
    try {
      let response = await deleteCheckItemm(checkItemid);
      if (response.status != 200) {
        throw new Error('404 erroritem not deleted')
      } else {
        let indexVal = -1;
        let answer = item.filter((i, index) => {
          if (i.id == checkItemid.id) {
            indexVal = index
          }
          return i.id != checkItemid.id
        });
        setItem(answer);

        newChkStates = chkStates.reduce((accumulator, current, index) => {
          if (index != indexVal) {
            accumulator.push(current)
          }
          return accumulator
        }, [])
        setChkStates(newChkStates);
      }
    } catch (e) {
      setError('404 Error - item not Deleted')
    }
  }

  return (
    <>
      {loading ?
        <Loading />
        :
        <Card >
          <ProgressBar chkStates={chkStates} />

          {item.map((chkItem, index) => (
            <Box key={chkItem.id} sx={{ display: "flex", justifyContent: "space-between", "&:hover .deleteBtn": { display: 'block' } }}  >
              
              <Box sx={{ display: "flex", alignItems: 'center', "&:hover .deleteBtn": { display: "flex", justifyContent: "center", alignItems: "center" } }}>
                <Checkbox  {...label} checked={chkStates[index]} onChange={() => handleToggle(index, chkItem)} />
                <Typography variant='h5' sx={{ ...chkStates[index] ? { textDecorationLine: "line-through" } : {} }}  >  {chkItem.name} </Typography>
              </Box>

              <DeleteOption deleteCheckItem={deleteCheckItem} chkItem={chkItem}  >
                <MoreHorizIcon sx={{ textAlign: "right", cursor: "pointer", display: "none", color: "black", }} className='deleteBtn' />
              </DeleteOption>
            </Box>
          ))}

          <Box>
            <Change ChkItemidList={work.id} functionName={'addNewChecklist'} start={'Create CheckItem'} text={'Enter CheckItem Name'} submitMatter={'Add'} item={item} setItem={setItem} setChkStates={setChkStates} chkStates={chkStates} />
          </Box>
          {error.length > 0 ? <Toast error={error} setError={setError} /> : ''}
        </ Card>
      }

    </>
  );
};

export default CheckItems; 