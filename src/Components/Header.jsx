import React from 'react'
import { AppBar, Toolbar, Typography } from '@mui/material';
import useStyles from '../Styles/styles';
import { Link, useNavigate } from 'react-router-dom';

const Header = () => {
    const classes = useStyles();
    const navigate = useNavigate();
    const goTo = () =>{
        navigate('/')
    }
    
    return (
        <div>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h4" onClick={() => goTo() } sx={{ color: "white", textDecoration: "none", border: "none" , cursor:"pointer" }} className={classes.title}>
                        Trello
                    </Typography>
                </Toolbar>
            </AppBar>
        </div>
    )
}

export default Header
