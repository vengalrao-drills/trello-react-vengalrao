import React from 'react'
import Header from './Header'
import {Typography } from '@mui/material';

const PageNotFound = () => {
  return (
    <div>
      <Header/>
      <Typography variant='h3'>
       404 Error -  Page Not Found
      </Typography>
      
    </div>
  )
}

export default PageNotFound
