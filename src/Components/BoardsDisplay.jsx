import React, { useState } from 'react'
import useStyles from '../Styles/styles'
import { Typography } from '@mui/material';
import { Box } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import AddNewBoard from '../MuiUsable/AddNewBoard.jsx';
import CloseIcon from '@mui/icons-material/Close';
import { Button } from '@mui/material';
import DeleteOption from '../MuiUsable/DeleteOption';
import Toast from '../MuiUsable/Toast.jsx';
import {deleteBoardId} from '../Data/key.js';
 

const BoardsDisplay = ({ PostBoardData, data, handleChange ,setData }) => {
    const classes = useStyles();
    const navigate = useNavigate();
    const gotTo = (id, board) => {
        navigate(`/boards/${id}`, { state: board.name })
    }
    let [error, setError] = useState('')

    const deleteIt = async  (id) => {        
        let response  =await deleteBoardId(id);
        if(response.status ==200){
            let value = data.filter(val=>val.id!= id)   ;
            setData( value  )  ;         
        }else{
            setError('Error 401 Bad Resquest - Board Not Deleted ');
        }
    }

    let boards = null;
    if (data) {
        boards = data.map((board) => {
            return (
                <Box sx={{cursor:"pointer", display: "flex", flexDirection: "row", alignItems: "baseline", "&:hover .CloseIcon": { opacity: "1" } }} key={board.id} className={classes.Allboards} >
                    <Typography sx={{ height: "100%" }} className={classes.boardsNames} variant="h4" onClick={() => gotTo(board.id, board)} >
                        {board.name}
                    </Typography>
                    <Button className='CloseIcon' sx={{ opacity: "1", color: "black", transition: "0.3s", '&:hover': { background: "white", transition:"all 0.5s" } }}  >
                        <DeleteOption sx={{ margin: "0", padding: "0" }} deleteIt={deleteIt} id={board.id}     >
                            <CloseIcon sx={{ color: "black" , opacity:"0"}} className='CloseIcon' />
                        </DeleteOption>
                    </Button>
                    {error.length >0 ? 
                    <Toast error={error} setError={setError} /> :''
                 }  
                </Box>
            );
        });
    }

    return (
        <Box className={classes.boardsContainer}>
           {boards}
            <Box className={`${classes.Allboards} ${classes.l} `}   >
                <AddNewBoard name='boardName' handleChange={handleChange} PostBoardData={PostBoardData} />
            </Box>
            {error.length >0 ?  <Toast  error={error} setError={setError} />  :""}
        </Box>
    )
}

export default BoardsDisplay