import React, { useEffect, useState } from 'react';
import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions'; 
import Typography from '@mui/material/Typography';
import { Box, Card } from '@mui/material'; 
import exportData from '../Data/key';
import CheckItems from './CheckItems';
import Toast from '../MuiUsable/Toast';
import Loading from '../MuiUsable/Loading'; 
import DeleteOption from '../MuiUsable/DeleteOption';
import AddList from '../MuiUsable/AddList';
import { getCheckListss , createCheckListt , deleteCheckListt } from '../Data/key';

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },
}));

export default function CheckListsDisplay({ children, cardId, cardName }) {
  const [open, setOpen] = React.useState(false);

  let { key, APIToken } = exportData;
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const [error, setError] = useState('')

  let [checkListData, setCheckListData] = useState([]);
  const [loading, setLoading] = useState(true)
  const getCheckLists =async  () => {
    try{
      let response = await getCheckListss(cardId)
      setCheckListData(response)
      setLoading(false)
    }catch(e){
      setError('404 Error - Checklists Data Not found')
    }
  }

  const createCheckList = async ( name) => {
    try{
      let response = await createCheckListt( cardId , name)
      setCheckListData(prev => [...prev, response])
    }catch(e){
      setError('404 Error - Cannot Create CheckList');
    }
  }

  const deleteCheckList = async (cardId, idCheckItem) => {
    try{
      let response = await deleteCheckListt(cardId, idCheckItem);
      if(response.status==200){
        setCheckListData(prev => prev.filter(p => p.id != idCheckItem))
      }else{
        throw new Error('not deleted')
      }
    }catch(e){
      setError('404 Error - Cannot Delete Checklist');      
    }
  }

  useEffect(() => {
    getCheckLists()
  }, [])

  return (
    <React.Fragment>
      <Box variant="outlined" sx={{ width: "100%", cursor: "pointer" }} onClick={() => { getCheckLists(); handleClickOpen() }}>
        {children}
      </Box>

      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        {loading ? <Loading /> :
          <>
            <DialogTitle sx={{ m: 1, p: 0, display: "flex", flexDirection: "row", alignItems: "center", width: 'lg' }} id="customized-dialog-title">
              <Typography variant='h3' sx={{ padding: "0.5rem 2.5rem", minWidth: "35rem", textAlign: "center" }} > {cardName}   </Typography>
            </DialogTitle>


            <Box sx={ {textAlign:"right" , marginRight:"4%" } }>

            <AddList createCheckList={createCheckList} setCheckListData={setCheckListData} textDisplay={'Enter new Checklist'}  submitMatter={'Add'} text={'Enter Checklist Name'} start={'Create Checklist'} functionName={'createCheckList'}>
            <Typography variant='h6'>
                Create CheckList
            </Typography>
            </AddList>
            </Box>
             {/* <Change close={'CLOSE'} classname='checklistsDisplay' createCheckList={createCheckList} submitMatter={'Add'} text={'Enter Checklist Name'} start={'Create Checklist'} functionName={'createCheckList'} /> */}

            {error.length > 0 ? <Toast error={error} setError={setError} /> : ""}

            <DialogContent dividers>
              {checkListData.map((work, index) => {
                return (
                  <Card sx={{ margin: "0rem 0.5rem 2rem 0.5rem", }} key={work.id}  >
                    <Box sx={{ padding: "0.5rem 0.5rem", display: "flex", justifyContent: "space-between", }} >
                      <Typography variant='h4' key={index}>
                        {work.name}
                      </Typography>
                      <Typography  variant="outlined"    sx={{ display: "flex", alignItems: "center" , border:"1px solid black", borderRadius:"0.5rem"  }}>
                        <DeleteOption deleteCheckList={deleteCheckList} cardId={cardId} workId={work.id} >
                           Archive
                        </DeleteOption>
                      </Typography>
                    </Box>
                    <Box>
                      <CheckItems  work={work} />
                    </Box>
                  </Card>
                )
              })}
            </DialogContent>

            <DialogActions sx={{ display: 'flex', justifyContent: "flex-end" }}>
              <Button autoFocus onClick={handleClose}>
                <Typography>Close </Typography>
              </Button>
            </DialogActions>
          </>
        }

      </BootstrapDialog>
    </React.Fragment>
  );
}









