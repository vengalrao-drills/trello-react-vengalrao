import * as React from 'react';
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import TextField from '@mui/material/TextField';
import useStyles from '../Styles/styles';
import Toast from './Toast'; 
import { Box } from '@mui/material';

export default function AddList({ children
  ,   submitMatter,
  text, start, functionName, textDisplay  , createCheckList
}) {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [openOnButtonClick, setOpenOnButtonClick] = React.useState(false);
  const open = Boolean(anchorEl);
  const [error , setError] = React.useState('')
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
    setOpenOnButtonClick(true);
  };
  const handleClose = () => {
    setAnchorEl(null);
    setOpenOnButtonClick(false);
    setNewCard('')
  };
  const close = () => {
    setNewCard('')
    handleClose();
  }


  const [newCard, setNewCard] = React.useState('');
  const handleChange = (e) => {
    setNewCard(e.target.value)
  }

  const allFunctions = (name) => {
    switch (name) {
      case 'createCheckList':
        createCheckList(newCard);
        setNewCard('')
        break;

      default:
        return ;
    }
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    if (functionName.length > 0) {
      allFunctions(functionName);
    }
    handleClose();
  }


  return (
    <div>
      <Button
        id="basic-button"
        aria-controls={open ? 'basic-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
      >
        {children}
      </Button>
      
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          'aria-labelledby': 'basic-button',
        }}
        onMouseEnter={() => setOpenOnButtonClick(false)}       >
        <MenuItem  >

          <form onSubmit={handleSubmit}  >
            <TextField sx={{ color: "black", margin: "1rem 0 0 0 " }} required value={newCard} id={'outlined'} label={textDisplay} variant="outlined" onChange={(e) => { handleChange(e) }} />
            <Box sx={{ margin: "1rem auto" }}>
              <Button variant="contained" type='submit' sx={{}}    >  {submitMatter} </Button>
              <Button sx={{ width: "10%", alignItems: 'flex-start' }} onClick={() => close()}>
                Close
              </Button>
            </Box>
          </form>

          {error.length>0 ?   <Toast error={error} setError={setError} />  :    ''}
        </MenuItem>
      </Menu>
    </div>
  );
}
