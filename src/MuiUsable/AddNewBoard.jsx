import * as React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
export default function AddNewBoard({ button, name, handleChange, PostBoardData }) {
    let dataButton = 'create';
    if (button) {
        dataButton = button;
    }
    
    const handleClickOpen = () => {
        setOpen(true);
    };

    const [open, setOpen] = React.useState(false);
    const handleClose = () => {
        setOpen(false);
    };

    return (
        <React.Fragment>
            <Button variant="outlined" sx={{ color: "white", height: "100%", width: "13rem", fontSize: "1rem"   }} onClick={handleClickOpen}>
                Create New Board
            </Button>
            <Dialog
                open={open}
                onClose={handleClose}
                PaperProps={{
                    component: 'form',
                    onSubmit: (event) => {
                        event.preventDefault();
                        PostBoardData();
                        handleClose();
                    },
                }}
            >
                <DialogContent >
                    <TextField
                        autoFocus
                        required
                        margin="dense"
                        name={name}
                        label="Board Name"
                        type='text'
                        fullWidth
                        variant="standard"
                        onChange={(e) => { handleChange(e) }}
                    />

                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Cancel</Button>
                    <Button type="submit"> {dataButton}  </Button>
                </DialogActions>
            </Dialog>
        </React.Fragment>
    );
}
