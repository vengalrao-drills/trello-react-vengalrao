import { Box } from '@mui/material'
import React, { useState } from 'react'
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';
import { Typography } from '@mui/material';
import TextField from '@mui/material/TextField';
import CloseIcon from '@mui/icons-material/Close';
import Toast from './Toast';
import { createCardd , addNewChecklistt , addNewListt } from '../Data/key'

const Change = ({ close, start, text, setCardsData, id, functionName, submitMatter, checkListData, setCheckListData, setItem, item, currentBoardId, setLists, createCheckList, classname, ChkItemidList, setChkStates, chkStates }) => {
    const [newCard, setNewCard] = useState('')
    const [newCardEntry, setNewCardEntry] = useState(true);
    const [error, setError] = useState('')
    let textDisplay = 'Enter ChecklistName  ';
    let closeVal = ''
    if (close) {
        closeVal = close;
    }
    if (text) {
        textDisplay = text;
    }

    const createCard = async () => {
        try {
            let response = await createCardd(newCard, id);
            setCardsData(prev => [...prev, response])
            setNewCard('');
        } catch (e) {
            setNewCard('');
            setError(' Error 404 -  Cannot create New Card');
        }
    }

    const addNewChecklist = async (ChkItemidList) => {
        try{
            let response = await addNewChecklistt(ChkItemidList , newCard);
            console.log('response' , response)
            let values = [...item, response]
            setItem(values)
            setNewCard('')
            let answer = [...chkStates, false];
            setChkStates(answer);

        }catch(e){
            setError('404 Error - Cannot post ');
            setNewCard('');
        }
    }

    const addNewList = async () => {
        try{
            let response = await addNewListt(currentBoardId , newCard)
            setNewCard('');
            setLists(prev => [...prev, response]);
        }catch(e){
            setNewCard('');
            setError('404 Error - cannot add NewList ');
        }
    }

    const createCheckListt = () => {
        createCheckList(newCard);
        setNewCard('')
    }


    const allFunctions = (functionName) => {
        switch (functionName) {
            case "createCard":
                createCard();
                break;
            case "addNewChecklist":
                addNewChecklist(ChkItemidList);
                break;
            case "addNewList":
                addNewList();
                break;
            case "createCheckList":
                createCheckListt();
                break;

            default:
                return
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        if (functionName.length > 0) {
            allFunctions(functionName)
        }
        setNewCardEntry(prev => !prev)
    }

    const handleChange = (e) => {
        setNewCard(e.target.value)
    }

    const changeView = () => {
        setNewCardEntry(prev => !prev)
    }

    return (
        <>
            <Box   >
                {newCardEntry ?
                    <Button onClick={() => changeView()} variant="text"    >
                        <Box><AddIcon sx={{ fontSize: "1.7rem", alignSelf: "self-start" }} />  </Box>
                        <Typography variant='h5' sx={{ textTransform: 'none', margin: "0 0 0 0.3rem", flexGrow: '1', textAlign: "start", width: "100%" }}  > {start}   </Typography>
                    </Button>
                    :
                    <>
                        <form onSubmit={handleSubmit}   >
                            <TextField className={classname ? `classes.${classname}` : ''} sx={{ color: "black", margin: "1rem 0 0 0 " }} required value={newCard} id={'outlined'} label={textDisplay} variant="outlined" onChange={(e) => { handleChange(e) }} />
                            <Box sx={{ margin: "1rem auto" }}>
                                <Button variant="contained" type='submit' sx={{ margin: "0 0.5rem" }}  >  {submitMatter} </Button>
                                <Button sx={{ width: "10%", alignItems: 'flex-start', padding: "0 1rem" }} onClick={() => { setNewCard(''); changeView() }}> <CloseIcon>  </CloseIcon>  </Button>
                            </Box>
                        </form>
                    </>
                }
                {error.length > 0 ? <Toast error={error} setError={setError} /> : ""}
            </Box>
        </>
    )
}

export default Change    