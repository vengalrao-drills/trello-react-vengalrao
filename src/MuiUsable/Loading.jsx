import React from 'react'
import CircularProgress from '@mui/material/CircularProgress';

const Loading = () => {
  return (
    <div>
      <CircularProgress  color="primary"  variant="indeterminate"  /> 
    </div>
  )
}

export default Loading
