import * as React from 'react';
import Snackbar from '@mui/material/Snackbar';

export default function AutohideSnackbar({error , setError}) {
  const [open, setOpen] = React.useState(false);
 

  const handleClose = ( reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setTimeout(()=>{
        setError('')
    }, 5000)

    setOpen(false);
  };

  React.useEffect(() => {
    if (error) {
      setOpen(true);
    }
  }, [error]);

  return (
    <div> 
      <Snackbar
        open={open}
        autoHideDuration={5000}
        onClose={handleClose}
        message={error}
      />
    </div>
  );
}
