import * as React from 'react';
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';
import { Typography } from '@mui/material';

export default function ProgressBar({chkStates}) {
  const [progress, setProgress] = React.useState(30);

  const checkProgress = ()=>{
    let count = 0;
    chkStates.forEach(state=>{
        if(state){
            count+=1;
        }
    })

    let value = (count / chkStates.length) * 100;
    if (value % 1 === 0) {
        value = value.toFixed(0);
    } else {
        value = value.toFixed(1);
    }
    
    setProgress(value)
  }
  React.useEffect(()=>{
        checkProgress();
  },[chkStates])
 
  return (
    <Box sx={{ display: 'flex', alignItems: 'center' }}>
       
        {chkStates.length>0  ?
        <>
         <Box sx={{ minWidth: 35 }}>
            <Typography variant="body2" color="text.secondary" sx={{margin:"0 1rem 0 0.5rem"}}> {progress}% </Typography>
            </Box>
            <Box sx={{ width: '100%' }}>
            <LinearProgress variant="determinate" value={progress}  sx={{margin:"0 1rem 0 0rem" , height:"0.5rem" , borderRadius:"0.2rem"}} />
            </Box>
        </>     
      : " " }
    </Box>
  );
}